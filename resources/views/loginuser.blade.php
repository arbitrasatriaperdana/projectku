<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href=" {{asset('assets/css/loginstyle.css')}}" rel="stylesheet">
<!------ Include the above in your HEAD tag ---------->

<div class="wrapper fadeInDown">
  <div id="formContent">
    <!-- Tabs Titles -->
    <h1>Login</h1>
            <!-- alert error -->
            @if(session()->has('success'))
                <div class="alert alert-success" style="color: green;font-weight:bold">
                    {{ session()->get('success') }}
                </div>
            @endif
            <!-- END::Alert error -->
            <!-- alert error -->
            @if(session()->has('error'))
                <div class="alert alert-danger" style="color: red;font-weight:bold">
                    {{ session()->get('error') }}
                </div>
            @endif

    <!-- Icon -->
    <div class="fadeIn first">
      <img src="http://danielzawadzki.com/codepen/01/icon.svg" id="icon" alt="User Icon" />
    </div>

    <!-- Login Form -->
    <form method ="GET" action="/proses">
      <input type="text" id="email" class="fadeIn second" name="email" placeholder="email">
      <input type="text" id="password" class="fadeIn third" name="password" placeholder="password">
      <input type="submit" class="fadeIn fourth" value="Log In">
    </form>

    <!-- Remind Passowrd -->
    <div id="formFooter">
      <a class="underlineHover" href="#">Forgot Password?</a>
    </div>

  </div>
</div>