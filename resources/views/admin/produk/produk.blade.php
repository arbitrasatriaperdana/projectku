@extends('MasterAdmin')

@section('content')
<br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data Produk</h3>
              </div>
              <div class="col col-xs-6 text-right">
              <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#ModalExample">
                      Tambah data
                </button>
              </div>
            </div>      
             <!-- BEGIN::MODAL TAmbAH DATA -->
             <div id="ModalExample" tabindex="1"class="modal fade">
            <!-- begin modal dialog -->
              <div class="modal-dialog">
              <!-- begin modal content -->
                <div class="modal-content">
                  <div class="modal-header">
                    <h4>Tambah Data Produk</h4>
                  </div>
                  <div class ="modal-body">
                  <form role="form" method="POST" action="/produk/post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Produk</label>
                        <input type="text" class="form-control" name="productName" id="productName" placeholder=" ">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jenis Produk</label>
                        <!-- <input type="text" class="form-control" name="productCategory" id="productCategory" placeholder=" "> -->
                        <select name="productCategory"  id="productCategory"class="form-control">
                          <option value="Blue Rim">Blue Rim</option>
                          <option value="Avatar">Avatar</option>
                          <option value="Red Cooper">Red Cooper</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Deskripsi Produk</label>
                        <textarea class="form-control" id="" name="productDesc" rows="3" ></textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Stok</label>
                        <input type="text" class="form-control" id="qty" name="qty" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Harga</label>
                        <input type="text" class="form-control" id="price" name="price" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Usia</label>
                        <input type="text" class="form-control" id="age" name="age" placeholder="">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Gambar</label>
                        <input type="file" class="form-control" id="productImg" name="productImg" required>
                      </div>
                      <button type="submit" class="btn btn-primary" id="insertProduct">Submit</button>                      
                    </form>

                  </div>
                </div>
                <!-- modal content end -->
              </div> 
              <!-- modal dialog end -->
            </div>
            <!-- END::MODAL TAmbAH DATA -->
                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">ID</th>
                            <th>Nama Ikan</th>
                            <th>Jenis Ikan</th>
                            <th>Deskripsi</th>
                            <th>Stok </th>
                            <th>Harga</th>
                            <th>Usia</th>
                            <th>Gambar Produk</th>
                        </tr> 
                      </thead>
                      <tbody>
                      @foreach($produk as $p)
                              <tr>
                                <td align="center">
                                  <a class="btn btn-default" data-toggle="modal" data-target="#modalUpdateProduct{{$p->id}}"><em class="fa fa-pencil"></em></a>
                                  <button class="btn btn-danger deleteProduct deleteProductId" value="{{$p->id}}"><em class="fa fa-trash"></em></a>
                                </td>
                                <td class="hidden-xs">{{$p->id}}</td>
                                <td>{{$p->product_name}}</td>
                                <td>{{$p->product_category}}</td>
                                <td>{{$p->product_desc}}</td>
                                <td>{{$p->qty}}</td>
                                <td>{{$p->age}}</td>
                                <td>{{$p->price}}</td>
                                <td><img src="{{asset('/gambar/'.$p->product_img)}}"class="img-thumbnail" width="75"></td>
                              </tr>
                               <!-- modal edit data -->
                        <div id="modalUpdateProduct{{$p->id}}" tabindex="1"class="modal fade">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h4>Edit Data Produk</h4>
                              </div>
                              <div class ="modal-body">
                                <!-- FORM UPDATE DATA -->
                                <form action="/produk/update/{{($p->id)}}" method="POST" enctype="multipart/form-data">
                                  @csrf
                                  <div class="form-group">
                                    <label for="exampleInputEmail1">Nama Produk</label>
                                    <input type="text" class="form-control" name="productName" id="" value="{{$p->product_name}}">
                                  </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Jenis Produk</label>
                                      <select class="browser-default custom-select" name="ProductCategory" id="">
                                        <option selected="" value="2">1</option>
                                          <option value="2">2</option>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Deskripsi Produk</label>
                                      <textarea class="form-control" id="" name="productDesc" rows="3" >{{$p->product_desc}}</textarea>
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Stok</label>
                                      <input type="text" class="form-control" id="qty" name="qty" value="{{$p->qty}}">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Harga</label>
                                      <input type="text" class="form-control" id="price" name="price" value="{{$p->price}}">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Usia</label>
                                      <input type="text" class="form-control" id="age" name="age" value="{{$p->age}}">
                                    </div>
                                    <div class="form-group">
                                      <label for="exampleInputEmail1">Gambar</label>
                                      <div>
                                        <input type="hidden" name="prevImage" value="{{$p->product_img}}">
                                        <img src="{{asset('/gambar/'.$p->product_img) }}"class="img-thumbnail" width="75" height="75">
                                      </div>
                                      <input type="file" class="form-control" id="productImg" name="productImg" required>
                                    </div>
                                  <button type="submit" class="btn btn-primary">Submit</button>     
                                </form>   
                              </div>
                            </div>
                            <!-- modal content end -->
                          </div> 
                          <!-- modal dialog end -->
                      @endforeach
                        </tbody>
                    </table>
                    {{$produk->links()}}
                  <div class="panel-footer">
                  
                  </div>
                </div>
            </div><!--panel -->
          </div><!--col 12 -->
        </div>


<script>

  </script>
@endsection