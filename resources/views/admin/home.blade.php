@extends('MasterAdmin')

@section('content')
<div class="row">
      <div class="col">
        <h2>Dashboard</h2>
        <div class="row">
        <div class="col">
                <div class="small-box bg-info">
                  <div class="inner">
                    <h4>Produk</h4>

                    <p>Kelola semua data produk pada website
                    </p>
                  </div>
                  <a href="/page_score" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>       
        </div>
        <div class="col">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h4>Pesanan</h4>

                    <p>Kelola semua data pesanan pada website
                    </p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>
                  <a href="/page_postingan" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>      
            </div>
        </div>
        <div class="row">
        <div class="col">
                <!-- small box -->
                <div class="small-box bg-danger">
                  <div class="inner">
                    <h4>Promo</h4>

                    <p>Kelola semua data promo pada website
                    </p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>
                  <a href="/page_fnb" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <div class="col">
                <!-- small box -->
                <div class="small-box bg-success">
                  <div class="inner">
                    <h4>User</h4>

                    <p>Kelola semua data user pada website
                    </p>
                  </div>
                  <div class="icon">
                    <i class="ion ion-stats-bars"></i>
                  </div>
                  <a href="/page_challenge" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
            </div>
        </div>



            <!-- ISIH ONO LE EROR LINK PAGE.E -->
            </div>
            <div class="col"><h2>Edit Profil Salatiga Betta Genetic</h2>
            
                      <form role="form" method="POST" action="#">
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputEmail1">Kontak</label>
                        <input type="text" class="form-control" name="Kontak" id="Kontak" value="">
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlTextarea1">Alamat</label>
                        <textarea class="form-control" id="Alamat" name="Alamat" rows="3" value="">}</textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Jam Buka</label>
                        <br>
                        
                        <div class="row">
                        <div class="col-6">
                        <label for="exampleInputEmail1">Senin</label>
                        <input type="text" class="form-control" name="Senin" id="Senin" value="">
                        <label for="exampleInputEmail1">Selasa</label>
                        <input type="text" class="form-control" name="Selasa" id="Selasa" value="">
                        <label for="exampleInputEmail1">Rabu</label>
                        <input type="text" class="form-control" name="Selasa" id="Selasa" value="">
                        </div>
                        
                        <div class="col-6">
                        <label for="exampleInputEmail1">Kamis</label>
                        <input type="text" class="form-control" name="Selasa" id="Selasa" value="">
                        <label for="exampleInputEmail1">Jumat</label>
                        <input type="text" class="form-control"name="Jumat" id="Jumat" value="">
                        <label for="exampleInputEmail1">Sabtu</label>
                        <input type="text" class="form-control" name="Sabtu" id="Sabtu" value="">
                        </div>
                      </div>
                      <br>
                      <button type="submit" class="btn btn-primary" id="updateInfo">Submit</button>                      
                    </form>
                    
                    </div>
@endsection