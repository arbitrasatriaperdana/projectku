@extends('MasterAdmin')
@section('content')
<br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data Detail Order</h3>
                <br>

              </div>
            <!-- END::MODAL TAmbAH DATA -->
                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">Nomor Produk</th>
                            <th>Nama Produk</th>
                            <th>Jumlah</th>
                            <th>Harga Satuan</th>
                        </tr> 
                      </thead>
                      <tbody>
                        @foreach($detailOrder as $o)
                        <tr>
                            <td></td>
                            <td>{{$o->id}}</td>
                            <td>{{$o->product_name}}</td>
                            <td>{{$o->jumlah}}</td>
                            <td>{{$o->price}}</td>
                        </tr>
                        </tbody>
                        @endforeach
</table>
                  <div class="panel-footer">
                    
                  </div>
                </div>
            </div><!--panel -->
          </div><!--col 12 -->
        </div>


@endsection