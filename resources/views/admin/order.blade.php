@extends('MasterAdmin')

@section('content')
<br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data Order</h3>
              </div>
            <!-- END::MODAL TAmbAH DATA -->
                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">Nomor Pesanan</th>
                            <th>Nama Pembeli</th>
                            <th>Metode Pembayaran</th>
                            <th>Total Harga</th>
                            <th>Status Order </th>
                        </tr> 
                      </thead>
                      <tbody>
                      @foreach($order as $o)
                              <tr>
                                <td align="center">
                                  <a type="button"class ="btn btn-info" href="/validasiemail/{{$o->id}}"><em class="">Kirim Validasi</em></a>
                                  <a type="button"class ="btn btn-info" href="/validasipengiriman/{{$o->id}}"><em class=""> Validasi </em></a>
                                  <a type="button" class="btn btn-danger  showDetail" href="/detailOrder/{{$o->id}} "><em class="">Lihat Pesanan</em></a>
                                </td>
                                <td class="hidden-xs">{{$o->id}}</td>
                                <td>{{$o->name}}</td>
                                <td>{{$o->metode_pembayaran}}</td>
                                <td>{{$o->total_harga}}</td>
                                <td>{{$o->status_order}}</td>
                                
                              </tr>
                               <!-- modal edit data -->
                          <!-- modal dialog end -->
                      @endforeach
                        </tbody>
</table>
{{$order->links()}}
                  <div class="panel-footer">
                    
                  </div>
                </div>
            </div><!--panel -->
          </div><!--col 12 -->
        </div>


<script>

  </script>
@endsection