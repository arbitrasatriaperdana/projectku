@extends('MasterAdmin')

@section('content')
<br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data Kategori</h3>
              </div>
              <div class="col col-xs-6 text-right">
              <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#ModalExample">
                      Tambah data
                </button>
              </div>
            </div>      
             <!-- BEGIN::MODAL TAmbAH DATA -->
             <div id="ModalExample" tabindex="1"class="modal fade">
            <!-- begin modal dialog -->
              <div class="modal-dialog">
              <!-- begin modal content -->
                <div class="modal-content">
                  <div class="modal-header">
                    <h4>Tambah Data Kategori</h4>
                  </div>
                  <div class ="modal-body">
                  <form role="form" method="POST" action="/kategori/post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama kategori</label>
                        <input type="text" class="form-control" name="categoryName" id="categoryName" placeholder=" ">
                      </div>
                      <button type="submit" class="btn btn-primary" id="insertCategory">Submit</button>                      
                    </form>

                  </div>
                </div>
                <!-- modal content end -->
              </div> 
              <!-- modal dialog end -->
            </div>
            <!-- END::MODAL TAmbAH DATA -->
            <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">No</th>
                            <th>Nama Kategori</th>
                        </tr> 
                      </thead>
                      <tbody>
                      @foreach($kategori as $kategori)
                              <tr>
                                <td align="center">
                                  <button class="btn btn-danger deleteCategories deleteCategoriesId" value="{{$kategori->id}}"><em class="fa fa-trash"></em></a>
                                </td>
                                <td class="hidden-xs">{{$kategori->id}}</td>
                                <td>{{$kategori->category_name}}</td>
                              </tr>
                            
                      @endforeach
                        </tbody>
                    </table>

                  <div class="panel-footer">
                    
                  </div>
                </div>
            </div><!--panel -->
          </div><!--col 12 -->
        </div>


<script>

  </script>
@endsection