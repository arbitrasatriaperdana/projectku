@extends('MasterAdmin')

@section('content')
<br>
      	<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data User</h3>
              </div>
              </div>
            </div>      
            <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">No</th>
                            <th>Username</th>
                            <th>Password</th>
                            <th>First name</th>
                            <th>Fast name</th>
                            <th>Kontak</th>
                            <th>Email</th>
                        </tr> 
                      </thead>
                      <tbody>
                      @foreach($user as $user)
                              <tr>
                                <td align="center">
                                  <button class="btn btn-danger deleteCategories deleteCategoriesId" value="{{$user->id}}"><em class="fa fa-trash"></em></a>
                                </td>
                                <td class="hidden-xs">{{$user->id}}</td>
                                <td>{{$user->name}}</td>
                                <td>{{$user->password}}</td>
                                <td>{{$user->first_name}}</td>
                                <td>{{$user->last_name}}</td>
                                <td>{{$user->no_telp}}</td>
                                <td>{{$user->email}}</td>
                              </tr>
                            
                      @endforeach
                        </tbody>
                    </table>

                  <div class="panel-footer">
                    
                  </div>
                </div>
            </div><!--panel -->
          </div><!--col 12 -->
        </div>


<script>

  </script>
@endsection