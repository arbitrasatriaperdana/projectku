@extends('MasterAdmin')
@section('content')
<div class="row">
          <div class="col-12">
           <div class="panel panel-default panel-table">
            <div class="row">
              <div class="col col-xs-6">
                <h3 class="panel-title">Data Promo</h3>
              </div>
              <div class="col col-xs-6 text-right">
              <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#ModalExample">
                      Tambah data
                </button>
              </div>
            </div>      
             <!-- BEGIN::MODAL TAmbAH DATA -->
             <div id="ModalExample" tabindex="1"class="modal fade">
            <!-- begin modal dialog -->
              <div class="modal-dialog">
              <!-- begin modal content -->
                <div class="modal-content">
                  <div class="modal-header">
                    <h4>Tambah Data Promo</h4>
                  </div>
                  <div class ="modal-body">
                  <form role="form" method="POST" action="/promo/add" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Promo</label>
                        <input type="text" class="form-control" name="promoName" id="promoName" placeholder=" ">
                      </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Gambar</label>
                        <input type="file" class="form-control" id="promoImg" name="promoImg" required>
                      </div>
                      <button type="submit" class="btn btn-primary" id="insertPromo">Submit</button>                      
                    </form>

                  </div>
                </div>
                <!-- modal content end -->
              </div> 
              <!-- modal dialog end -->
            </div>
            <!-- END::MODAL TAmbAH DATA -->
                    <table class="table table-striped table-bordered table-list">
                      <thead>
                        <tr>
                            <th><em class="fa fa-cog"></em></th>
                            <th class="hidden-xs">ID</th>
                            <th>Nama Promo</th>
                            <th>Gambar Promo</th>
                        </tr> 
                      </thead>
                      <tbody>
                      @foreach($promo as $p)
                              <tr>
                                <td align="center">
                                  <a class="btn btn-default" data-toggle="modal" data-target="#modalUpdateProduct{{$p->id}}"><em class="fa fa-pencil"></em></a>
                                  <button class="btn btn-danger deletePromo deletePromoId" value="{{$p->id}}"><em class="fa fa-trash"></em></a>
                                </td>
                                <td class="hidden-xs">{{$p->id}}</td>
                                <td>{{$p->promo_name}}</td>
                                <td><img src="{{asset('/gambar/promo/'.$p->promo_img)}}"class="img-thumbnail" width="100"></td>
                              </tr>
                               <!-- modal edit data -->
                          <!-- modal dialog end -->
                      @endforeach
                        </tbody>
                    </table>
                  <div class="panel-footer">
                    
                  </div>
                </div>
            </div><!--panel -->
          </div><!--col 12 -->
        </div>


<script>

  </script>
@endsection