<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin Website Salatiga Betta Genetic</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.0.3/leaflet.css" type="text/css" crossorigin="">
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.js'></script>
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.1.1/mapbox-gl.css' rel='stylesheet' />
    <link href="{{asset('assets/css/style-no5-login.css')}}" rel="stylesheet" type="text/css">
</head>
<body>

    <main>
        <div class="main-container">
            <h1>Login Admin</h1>
            <!-- alert error -->
            @if(session()->has('success'))
                <div class="alert alert-success" style="color: green;font-weight:bold">
                    {{ session()->get('success') }}
                </div>
            @endif
            <!-- END::Alert error -->
            <!-- alert error -->
            @if(session()->has('error'))
                <div class="alert alert-danger" style="color: red;font-weight:bold">
                    {{ session()->get('error') }}
                </div>
            @endif
            <!-- END::Alert error -->
            <form method="GET" action="login/admin">
                @csrf
                <div class="container-login">
                    <p>Username</p>
                        <input type="text" name="username" id="username" placeholder="Type Your Username">
                    <p>Password</p>
                        <input type="password" name="password" id="password" placeholder="Type Your Password">
                        <input type="submit" value="LOGIN">
                </div>
            </form>
        </div>
    </main>
    
</body>
</html>