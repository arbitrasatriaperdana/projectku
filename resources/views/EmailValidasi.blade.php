<!DOCTYPE html>
<html>
<head>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>websitepercobaan.com</title>
</head>

<body>
<p>Pesanan anda dengan kode pesanan .... telah diproses</p>
<p>Berikut adalah daftar item yang anda beli :</p>
    <table class="table table-bordered">
    <thead>
    <tr>
      <th >Nama Produk</th>
      <th >Qty</th>
      <th >Harga</th>
    </tr>
  </thead>
  <tbody>
  @foreach($detailOrder as $orderdetail)
    <tr>
      <td>{{$orderdetail->product_name}}</td>
      <td>{{$orderdetail->jumlah}}</td>
      <td>{{$orderdetail->price}}</td>
    </tr>
    @endforeach 
  </tbody>
    </table>

   <p>Untuk pembayaran melalui bank harap bayar melalui 6044-01-XXXXX-XX-X Atas Nama Salatiga Betta Genetic<p>
   <p>Untuk pembayaran melalui e-wallet (OVO,DANA) harap bayar melalui 085840888987 Atas Nama Arbitra<p>
    <p>Setelah melakukan pembayaran harap upload bukti bayar di <a href ="http://127.0.0.1:8000/validasi">sini </a></p>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</html>

