@extends('frontEnd.Layout')
@section('UserContent')
<main class="ps-main">
      <div class="test">
        <div class="container">
          <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 ">
                </div>
          </div>
        </div>
      </div>
      <div class="ps-product--detail pt-60">
        <div class="ps-container">
          <div class="row">
            <div class="col-lg-10 col-md-12 col-lg-offset-1">
              <form action="{{route('keranjang.add')}}" method="post">
              @csrf
              <div class="ps-product__thumbnail">
                <div class="ps-product__preview">
                </div>
                <div class="ps-product__image">
                  <div class="item"><img class="zoom" src="{{asset('/gambar/'.$Produkdetail->product_img)}}" alt="" data-zoom-image="{{asset('/gambar/'.$Produkdetail->product_img)}}"></div>
                </div>
              </div>
              <div class="ps-product__thumbnail--mobile">
                <div class="ps-product__main-img"><img src="{{asset('/gambar/'.$Produkdetail->product_img)}}" alt=""></div>
                <div class="ps-product__preview owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000" data-owl-gap="20" data-owl-nav="true" data-owl-dots="false" data-owl-item="3" data-owl-item-xs="3" data-owl-item-sm="3" data-owl-item-md="3" data-owl-item-lg="3" data-owl-duration="1000" data-owl-mousedrag="on"><img src="images/shoe-detail/1.jpg" alt=""><img src="images/shoe-detail/2.jpg" alt=""><img src="images/shoe-detail/3.jpg" alt=""></div>
              </div>
              <div class="ps-product__info">
                <input type="hidden" name="product_id" value="{{$Produkdetail->id}}">
                <h1>{{$Produkdetail->product_name}}</h1>
                <p class="ps-product__category"><a href="#"> {{$Produkdetail->product_category}}</a>
                <h4>Stok : {{$Produkdetail->qty}}</h4>
                
                <div class="form-group">
                    <input class="form-control" type="number" name="qty">
                  </div>
                  <h1 class="ps-product__price">Harga : {{$Produkdetail->price}}</h3>
                <div class="ps-product__shopping"><button class="ps-btn mb-10" type=submit>Add to cart<i class="ps-icon-next"></i></button>
                </div>
              </div>
              </form>
              <div class="clearfix"></div>
              <div class="ps-product__content mt-50">
                <ul class="tab-list" role="tablist">
                  <li class="active"><a href="#tab_01" aria-controls="tab_01" role="tab" data-toggle="tab">Deskripsi</a></li>
                </ul>
              </div>
              <div class="tab-content mb-60">
                <div class="tab-pane active" role="tabpanel" id="tab_01">
                  <p>{{$Produkdetail->product_desc}}</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
