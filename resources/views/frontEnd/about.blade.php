@extends('frontEnd.Layout')
@section('UserContent')
<main class="ps-main">
      <div class="ps-checkout pt-80 pb-80">
        <div class="ps-container">
          <form class="ps-checkout__form" action="/ordersuccess" method="post">
          @csrf
            <div class="row">
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
                    <div class="ps-checkout__billing">
                    <img src="{{asset('assets/images/betta.jpg')}}" width ="500"alt="">

                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                      <h3>Salatiga Betta Genetic</h3>
                      <br>
                      <p class="text-justify"><font color="black">Salatiga Betta Genetic merupakan tempat pembibitan ikan cupang yang beralamat di
                      Jl. Sukowati, Kalicacing, Kec. Sidomukti, Kota Salatiga. Salatiga Betta Genetic menjual produk ikan cupang berjenis Red Koi Cooper,
                      Avatar,dan juga ikan cupang berjenis Blue Rim.
                      </font></p>
                      <br>
                      <h3> Jam Buka </h3>
                      <table class="table ">
                        <thead>
                            <tr>
                                <th>Hari</th>
                                <th>Jam</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Senin</td>
                                <td>09.00 - 20.00</td>
                            </tr>
                            <tr>
                                <td>Selasa</td>
                                <td>09.00 - 20.00</td>
                            </tr>
                            <tr>
                                <td>Rabu</td>
                                <td>09.00 - 20.00</td>
                            </tr>
                            <tr>
                                <td>Kamis</td>
                                <td>09.00 - 20.00</td>
                            </tr>
                            <tr>
                                <td>Jumat</td>
                                <td>09.00 - 20.00</td>
                            </tr>
                            <tr>
                                <td>Sabtu</td>
                                <td>09.00 - 20.00</td>
                            </tr>
                            <tr>
                                <td>Minggu</td>
                                <td>09.00 - 20.00</td>
                            </tr>
                        </tbody>
                        </table>

                  </div>
            </div>
          </form>
        </div>
      </div>
@endsection