@extends('frontEnd.Layout')
@section('UserContent')
<main class="ps-main">
      <div class="ps-checkout pt-80 pb-80">
        <div class="ps-container">
          <form class="ps-checkout__form" action="/ordersuccess" method="post">
          @csrf
            <div class="row">
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
                    <div class="ps-checkout__billing">
                      <h3>Rincian Pembayaran</h3>
                            <div class="form-group form-group--inline">
                              <label>Nama Pertama<span></span>
                              </label>
                              <input class="form-control" type="text"  value="{{$user->first_name}}">
                            </div>
                            <div class="form-group form-group--inline">
                              <label>Nama Terakhir<span>*</span>
                              </label>
                              <input class="form-control" type="text"  value="{{$user->last_name}}">
                            </div>
                            <div class="form-group form-group--inline">
                              <label>Email<span>*</span>
                              </label>
                              <input class="form-control" type="email"  value="{{$user->email}}">
                            </div>
                            <div class="form-group form-group--inline">
                              <label>Nomor Telepon<span>*</span>
                              </label>
                              <input class="form-control" type="text"  value="{{$user->no_telp}}">
                            </div>
                            <div class="form-group form-group--inline">
                            <div class="ps-shipping">
                              <label>Alamat<span>*</span>
                              </label>
                              <a class="ps-remover" id="" href="/alamatuser">Ubah alamat</a>
                            </div>
                            </div>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                    <div class="ps-checkout__order">
                      <header>
                        <h3>Rincian Produk </h3>
                      </header>
                      <div class="content">
                        <table class="table ps-checkout__products">
                          <thead>
                            <tr>
                              <th class="text-uppercase">Produk</th>
                              <th class="text-uppercase">Jumlah</th>
                              <th class="text-uppercase">Harga</th>
                            </tr>
                          </thead>
                          <tbody>
                          @foreach($Barang as $b)
                            <tr>
                              <td>{{$b->product_name}}</td>
                              <td>{{$b->total}}</td>
                              <td>{{$b->price * $b->total}}</td>
                            </tr>
                            @endforeach
                          </tbody>
                        </table>
                        <div class="text-uppercase"><h3>Total :  {{$SubHargaTotal}}</h3></div>
                      </div>
                      <footer>
                        <h3>Metode Pembayaran</h3>
                        <div class="form-group cheque">
                          <div class="ps-radio">
                            <input class="form-control" type="radio" id="rdo01" name="payment" value="Bank Transfer"checked>
                            <label for="rdo01">Pembayaran Via transfer bank</label>
                            <p></p>
                          </div>
                        </div>
                        <div class="form-group paypal">
                          <div class="ps-radio ps-radio--inline">
                            <label for="rdo02">Pembayaran Via e-wallet</label>
                          </div>
                          <ul class="ps-payment-method">
                            <li><a href="#"><img src="{{asset('assets/images/ovo.png')}}" alt=""></a></li>
              
                            <li><a href="#"><img src="{{asset('assets/images/dana.jpg')}}" alt=""></a></li>
                          </ul>
                          <input class="ps-btn ps-btn--fullwidth" type="submit">
                        </div>
                      </footer>
                    </div>
                    <!-- <div class="ps-shipping">
                      <h3>FREE SHIPPING</h3>
                     
                    </div> -->
                  </div>
            </div>
          </form>
        </div>
      </div>
@endsection