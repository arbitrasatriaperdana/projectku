@extends('frontEnd.Layout')
@section('UserContent')
<main class="ps-main">
      <div class="ps-checkout pt-80 pb-80">
        <div class="ps-container">
          <form class="ps-checkout__form" action="/ordersuccess" method="post">
          @csrf
            <div class="row">
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
                    <div class="ps-checkout__billing">
                      <h3>Form Validasi</h3>
                            <div class="form-group form-group--inline">
                              <label>id Order<span></span></label>
                              <div class="ps-product__block ps-product__size">
                                <select class="ps-select selectpicker">
                                  @foreach($getIdOrder as $getOrder)
                                    <option value="{{$getOrder->id}}">{{$getOrder->id}} {{$getOrder->created_at}}</option>
                                    @endforeach
                              
                                </select>
                            </div>
                            </div>
                            <div class="form-group form-group--inline">
                              <label>Upload Bukti bayar</label>
                              <input type="file" class="form-control" id="Bukti" name="Bukti" required>
                            </div>
                            
                            
                            
                            <button type="submit" class="ps-btn ps-btn" id="">Kirim</button>
                    </div>
                  </div>
            </div>

          </form>

        </div>

      </div>
@endsection