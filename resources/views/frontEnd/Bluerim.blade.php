@extends('frontEnd.Layout')
@section('UserContent')
<main class="ps-main">
      <div class="ps-products-wrap pt-80 pb-80">
        <div class="ps-products" data-mh="product-listing">
          <div class="ps-product-action">
            <div class="ps-product__filter">
              <select class="ps-select selectpicker">
                <option value="1">Shortby</option>
                <option value="2">Name</option>
                <option value="3">Price (Low to High)</option>
                <option value="3">Price (High to Low)</option>
              </select>
            </div>
            <div class="ps-pagination">
              <ul class="pagination">
                <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="ps-product__columns">
          @foreach($produklist as $produk)
            <div class="ps-product__column">
              <div class="ps-shoe mb-30">
                <div class="ps-shoe__thumbnail"><a class="ps-shoe__favorite" href="#"><i class="ps-icon-heart"></i></a><img src="{{asset('/gambar/'.$produk->product_img)}}" alt=""><a class="ps-shoe__overlay" href="/detail/{{$produk->id}}"></a>
                </div>
                <div class="ps-shoe__content">
                  <div class="ps-shoe__detail"><a class="ps-shoe__name" href="#">{{$produk->product_name}}</a>
                    <p class="ps-shoe__categories"><a href="#">{{$produk->product_category}}</a></p><span class="ps-shoe__price"> Rp : {{$produk->price}}</span>
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
          <div class="ps-product-action">
            <div class="ps-pagination">
              <ul class="pagination">
                <li><a href="#"><i class="fa fa-angle-left"></i></a></li>
                <li class="active"><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">...</a></li>
                <li><a href="#"><i class="fa fa-angle-right"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <div class="ps-sidebar" data-mh="product-listing">
          <aside class="ps-widget--sidebar ps-widget--category">
            <div class="ps-widget__header">
              <h3>Category</h3>
            </div>
            <!-- <div class="ps-widget__content">
              <ul class="ps-list--checked">
                <li class="current"><a href="product-listing.html">Life(521)</a></li>
                <li><a href="product-listing.html">Running(76)</a></li>
                <li><a href="product-listing.html">Baseball(21)</a></li>
                <li><a href="product-listing.html">Football(105)</a></li>
                <li><a href="product-listing.html">Soccer(108)</a></li>
                <li><a href="product-listing.html">Trainning & game(47)</a></li>
                <li><a href="product-listing.html">More</a></li>
              </ul>
            </div> -->
          </aside>

          <div class="ps-sticky desktop">
          </div>
        </div>
      </div>
@endsection