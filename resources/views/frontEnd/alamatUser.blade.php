@extends('frontEnd.Layout')
@section('UserContent')
<br><br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    <form method="POST" action="">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Provinsi') }}</label>

                            <div class="col-md-6">
                            <select class="form-control form-control-lg">
                                    <option>Pilih Provinsi</option>
                            </select>
                            </div>
                        </div>
<div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Kota') }}</label>


                            <div class="col-md-6">
                            <select class="form-control form-control-lg">
                                    <option>Pilih Kota</option>
                            </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="first_name" class="col-md-4 col-form-label text-md-right">Kecamatan</label>

                            <div class="col-md-6">
                            <select class="form-control form-control-lg">
                                    <option>Pilih Kecamatan</option>
                            </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="kode_pos" class="col-md-4 col-form-label text-md-right">{{ __('Kode Pos') }}</label>

                            <div class="col-md-6">
                            <input id="kode_pos" type="kode_pos" class="form-control " name="kode_pos" value="" required autocomplete="kode_pos">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kode_pos" class="col-md-4 col-form-label text-md-right">{{ __('Masukan Alamat Lengkap') }}</label>

                            <div class="col-md-6">
                            <input id="kode_pos" type="kode_pos" class="form-control " name="kode_pos" value="" required autocomplete="kode_pos">
                            </div>
                        </div>


                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Submit') }}
                                </button>
                              

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<br><br>
@endsection