@extends('frontEnd.Layout')
@section('UserContent')
<main class="ps-main">
      <div class="ps-content pt-80 pb-80">
        <div class="ps-container">
          <div class="ps-cart-listing">
            <table class="table ps-cart__table">
              <thead>
                <tr>
                  <th>Id Order</th>
                  <th>Total Harga</th>
                  <th>Metode Pembayaran</th>
                  <th>Status</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              
              @foreach($Order as $order)
                <tr>
                  <td>{{$order->id}}</td>
                  <td>{{$order->total_harga}}</td>
                  <th>{{$order->metode_pembayaran}}</th>
                  <th>{{$order->status_order}}</th>
                  <th><a type="button" class="btn btn-danger  showDetail" href="/history/detail/{{$order->id}} "><em class="">Lihat Pesanan</em></a></th>
                </tr>
              @endforeach
              </tbody>
            </table>

          </div>
        </div>
      </div>
@endsection