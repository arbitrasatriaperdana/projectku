@extends('frontEnd.Layout')
@section('UserContent')
<main class="ps-main">
<div class="ps-section--offer">
        @foreach($promoshow as $promo)
        <div class="ps-column"><a class="ps-offer" href="#"><img src="{{asset('/gambar/promo/'.$promo->promo_img)}}" alt="" width="900" height="400"></a></div>
        @endforeach
      </div>


      <div class="ps-section--features-product ps-section masonry-root pt-100 pb-100">
        <div class="ps-container">
          <div class="ps-section__header mb-50">
            <h3 class="ps-section__title" data-mask="features">Produk Yang Tersedia</h3>
          </div>
          <div class="ps-section__content pb-50">
            <div class="masonry-wrapper" data-col-md="4" data-col-sm="2" data-col-xs="1" data-gap="30" data-radio="100%">
              <div class="ps-masonry">
                <div class="grid-sizer"></div>
				@foreach($produkshow as $produk)
                <div class="grid-item nike">
                  <div class="grid-item__content-wrapper">
                    <div class="ps-shoe mb-30">
                      <div class="ps-shoe__thumbnail"><a class="ps-shoe__favorite" href="#"><i class="ps-icon-heart"></i></a><img src="{{asset('/gambar/'.$produk->product_img)}}" alt=""><a class="ps-shoe__overlay" href="/detail/{{$produk->id}}"></a>
                      </div>
                      <div class="ps-shoe__content">
                        <div class="ps-shoe__variants">
                          <div class="ps-shoe__variant normal"><img src="{{asset('/gambar/'.$produk->product_img)}}" alt=""></div>
                        </div>
                        <div class="ps-shoe__detail"><a class="ps-shoe__name" href="#">{{$produk->product_name}}</a>
                          <p class="ps-shoe__categories"><a href="#">{{$produk->product_name}}</a></p><span class="ps-shoe__price"> {{$produk->price}}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
				@endforeach
              </div>
            </div>
          </div>
        </div>
      </div>

        </div>
      </div>
    
	@endsection