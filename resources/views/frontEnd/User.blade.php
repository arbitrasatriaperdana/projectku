@extends('frontEnd.Layout')
@section('UserContent')
<main class="ps-main">
      <div class="ps-checkout pt-80 pb-80">
        <div class="ps-container">
          <form class="ps-checkout__form" action="/updateuser" method="post">
          @csrf
            <div class="row">
                  <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 ">
                    <div class="ps-checkout__billing">
                      <h3>Detail User</h3>
                      @foreach($viewUser as $user)
                            <div class="form-group form-group--inline">
                              <label>Nama Pertama<span></span>
                              </label>
                              <input class="form-control" type="text" disabled placeholder="{{$user->first_name}}">
                            </div>
                            <div class="form-group form-group--inline">
                              <label>Nama Terakhir<span>*</span>
                              </label>
                              <input class="form-control" type="text" disabled placeholder="{{$user->last_name}}">
                            </div>
                            <div class="form-group form-group--inline">
                              <label>Email <span>*</span>
                              </label>
                              <input class="form-control" type="email" disabled placeholder="{{$user->email}}">
                            </div>
                            <div class="form-group form-group--inline">
                              <label>Nomor Telepon<span>*</span>
                              </label>
                              <input class="form-control" type="text" disabled placeholder="{{$user->no_telp}}">
                            </div>
                            <div class="form-group form-group--inline">
                            <div class="ps-shipping">
                              <label>Address<span>*</span>
                              </label>
                              <a class="ps-remover" id="" href="/alamatuser">Ubah alamat</a>
                            </div>
                            </div>
                          @endforeach
                            <button type="submit" class="ps-btn ps-btn" id="">Update</button>
                    </div>
                  </div>
                  <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                  <a href="#"><img src="{{asset('assets/images/person.png')}}" alt=""></a>
                  <br><br>
                  <a class="ps-btn ps-btn--fullwidth" href="/validasi" >Kirim Validasi</a>
                  <br><br>
                  <a class="ps-btn ps-btn--fullwidth" id="" href="/history" >Riwayat Order</a>

                  </div>
            </div>

          </form>

        </div>

      </div>

@endsection