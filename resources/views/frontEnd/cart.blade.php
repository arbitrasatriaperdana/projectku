@extends('frontEnd.Layout')
@section('UserContent')
<main class="ps-main">
      <div class="ps-content pt-80 pb-80">
        <div class="ps-container">
          <div class="ps-cart-listing">
            <table class="table ps-cart__table">
              <thead>
                <tr>
                  <th>Nama Produk</th>
                  <th>Harga</th>
                  <th>Jumlah</th>
                  <th>Total</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
              @foreach($showKeranjang as $show)
                <tr>
                  <td><a class="ps-product__preview" href="product-detail.html">
                  <img class="mr-15" src="{{asset('/gambar/'.$show->product_img)}}" width="120" height="120" alt=""> {{$show->product_name}}</a></td>
                  <td>{{$show->price}}</td>
                  <td>
                    <div class="form-group--number">
                      <input class="form-control" type="text" value="{{$show->total}}">
                    </div>
                  </td>
                  <td>{{$show->price * $show->total}}</td>
                  <td>
                  <form action="{{ route('keranjang.delete',['id'=> $show->id]) }}" method="POST" onsubmit="return confirm('Hapus barang {{ $show->product_name }}?');" style="display: inline-block;">
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="ps-remover" id="deleteCartid">HAPUS</button>
                    </form>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
            <div class="ps-cart__actions">
              <div class="ps-cart__total">
                <h3>Total: <span> {{$hargabarang}}</span></h3><a class="ps-btn" href="{{route('order.viewadd')}}">Process to checkout<i class="ps-icon-next"></i></a>
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection