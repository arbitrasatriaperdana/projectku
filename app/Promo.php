<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promo extends Model
{
    public $table='promo';
    protected $primaryKey = 'id';
}
