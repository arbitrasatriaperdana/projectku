<?php

namespace App;


use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
class ValidasiPengiriman extends Mailable
{
    public $order;
    public $detailOrder;

        /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($order,$detailOrder)
    {
        $this->order = $order;
        $this->detailOrder = $detailOrder;
    }
 
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
       return $this->from('arbitrasatriaperdana@gmail.com')
                   ->view('ValidasiPengirimanMail')
                   ->with([
                       'order'=>$this->order,
                       'detailOrder' =>$this->detailOrder,
                    ]);
    }
}
