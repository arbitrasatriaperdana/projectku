<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Produk extends Model
{
    public $table='product';
    protected $primaryKey = 'id';
}
