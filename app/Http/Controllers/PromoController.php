<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Promo;
use App\Media;
use File;
Use \Carbon\Carbon;

class PromoController extends Controller
{
    public function index()
    {
        $promo = Promo::select('id','promo_name','promo_img')->paginate(5);
        return view('admin/promo', compact('promo'));
    }

    public function tambah(Request $request)
    {
        $promoname      = $request->promoName;
        $image          = $request->promoImg;
        $CategoryImg    =  'promo';
        $originalName   = $image->getClientOriginalName();
        $timestamp      = Carbon::now()->format('YmdHisu'); 
        $timeOriginalName = $timestamp.'_'.$originalName;
        $image->move(public_path()."/gambar/promo", $timeOriginalName);

        $newpromo = new Promo;
        $newpromo -> promo_name     =   $request->promoName;
        $newpromo -> promo_img      =   $timeOriginalName;
        $newpromo -> save();

        $newImage = new Media;
        $newImage->original_name = $timeOriginalName;
        $newImage->image_name = $promoname;
        $newImage->image_category = $CategoryImg;
        // dd($CategoryImg);
        $newImage->save();
        return redirect()->back()->with('success', 'berhasil Input Data!');
    }
    public function destroy(Request $request)
    {
        $id = $request->deletePromoId;
        // dd($id);
        $selectedData = Promo ::where('id', $id)->first();
        $imageName = $selectedData->promo_img;
        $oldDestinationPath = public_path().'/gambar/promo/'.$imageName;
        $newDestinationPath = public_path().'/delete/promo/'.$imageName;
        $move = File::move($oldDestinationPath, $newDestinationPath);
        if($move){
            $deleteFile = Promo::where('id',$id)->delete();
            if($deleteFile){
                return [
                    'result' => 'success',
                    'message' => 'Berhasil menghapus data!',
                ];
            }else{
                return [
                    'result' => 'error',
                    'message' => 'Error saat menghapus data!',
                ];
            }
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat menghapus gambar!',
            ];
        }
        return redirect()->back();
    }
    public function Update()
    {
        $prevImage = $request->prevImage;
        $oldDestinationPath = public_path().'/gambar/'.$prevImage;
        $newDestinationPath = public_path().'/delete/promo/'.$prevImage;
        if($oldDestinationPath == true){
            $move = File::move($oldDestinationPath, $newDestinationPath);
            // dd($move);
        }else{
            return [
                'result' => 'error',
                'message' => 'Kesalahan saat mengedit gambar!',
            ];
        }
        $image = $request->productImg;
        $originalName = $image->getClientOriginalName();
        $timestamp = Carbon::now()->format('YmdHisu'); 
        $timeOriginalName = $timestamp.'_'.$originalName;
        $moved = $image->move(public_path()."/gambar", $timeOriginalName);
        $editPromo = promo::find($id);
        $editPromo -> promo_name    =   $request->promoName;
        $editPromo -> product_img   =   $timeOriginalName;
        $editPromo ->save();
        return redirect()->back()->with('success', 'berhasil edit Data Produk!');
    }
}
