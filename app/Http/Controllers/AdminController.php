<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin;
use Redirect;

class AdminController extends Controller
{
    public Function view(){
        return view('/admin/home');
    }
    public function viewLogin()
    {
        return view('/admin/LoginAdmin');
    }
    public function loginAdmin(Request $request){
        $adminName = $request->username;
        $password = $request->password;

        $loginAdmin = Admin::where('admin_name', $adminName)->where('password', $password)->first();
        
        if($loginAdmin = true){
            session(['berhasil_login' => true]);
            $url = 'http://127.0.0.1:8000/dashboard/';
            return Redirect::to($url)->with('success', 'Selamat datang '.$adminName);
        }
        return redirect()->back()->with('error', 'Please check your data again!');
    }
    public function logoutAdmin(Request $request){
        $request->session()->flush();
        return redirect('/loginAdmin');//
    
    }
}
