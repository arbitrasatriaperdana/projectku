<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Order;
use App\Order_Detail;
use App\User;
use App\Produk;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function index(){
        
    }

    public function viewtambah(){
        $Barang = Cart:: join('product','Product.id','=','Cart.product_id')
                            -> select('Cart.total','Product.product_name','Product.product_img','Product.price')
                            ->where('Cart.user_id', Auth::id())->get();
        $user = User:: select('id','first_name','last_name','email','no_telp')
                            ->where('id',Auth::id())->get()->first();
                            // dd($user);

        $SubHargaTotal = 0;
        foreach ($Barang as $barang){
            $Harga = $barang->price * $barang->total;
            $SubHargaTotal = $SubHargaTotal + $Harga;
        }

        return view('frontEnd/checkout',compact('Barang', 'SubHargaTotal','user'));
    }

    public function tambah(Request $request){
        $Barang = Cart:: join('product','product.id','=','Cart.product_id')
                            -> select('Product.id','Cart.total','Product.product_name','Product.product_img','Product.price')
                            ->where('Cart.user_id', Auth::id())->get();

        $SubHargaTotal = 0;
        foreach ($Barang as $barang){
            $Harga = $barang->price * $barang->total;
            $SubHargaTotal = $SubHargaTotal + $Harga;
        }

        $order = new Order;
        $order->user_id = Auth::id();
        $order->metode_pembayaran = $request->payment;
        $order->total_harga = $SubHargaTotal;
        $order->status_order = 'Belum Dibayar';
        $order->save();
        //kurang notelp address

        foreach ($Barang as $barang){
            $Order_Detail = new Order_Detail;
            $Order_Detail->order_id = $order->id;
            $Order_Detail->product_id = $barang->id;
            $Order_Detail->jumlah = $barang->total;
            $Order_Detail->harga = $barang->price * $barang->total;
            $Order_Detail->save();
        }
        
        return redirect()->route('dashboard');
    }
//ADMIN PAGE ORDER
    public function viewAdmin()
    {
        $order= Order:: join('users','users.id','=','order.user_id')
                      ->Select('order.id','users.name','order.metode_pembayaran','order.total_harga','order.status_order')
                      ->orderBy('order.created_at','desc')
                    ->paginate(5);
        $lihatOrder= Order::join('detail_order','detail_order.order_id','=','order.id')
                          ->join('product','product.id','=','order.product_id')
                          ->select('order.id','product.product_name','detail_order.jumlah','detail_order.harga');
        
        return view('admin/order',compact('order','lihatOrder'));
    }
    public function viewDetail (request $request,$id)
    {
        
        $dataOrder = order :: select('id')
                             ->where('id',$id)->first();
        
        $detailOrder= Order_Detail::join('product','Order_detail.product_id','=','product.id')
                             ->where('Order_detail.order_id',$dataOrder->id)
                             ->select('Order_Detail.jumlah','product.product_name','product.price','product.id')
                             ->get();
                            
          //eror ra nampil                   
        return view('admin/orderdetail',compact('detailOrder','dataOrder'));
    }
    public function history()
    {
        $Order= Order:: join('users','users.id','=','order.user_id')
                      ->Select('order.id','users.name','order.metode_pembayaran','order.total_harga','order.status_order')
                      ->where('users.id', Auth::user()->id)
                    ->paginate(3);

        return view('frontEnd/history_order',compact('Order'));
    }

    public function history_detail($id)
    {
        $lihatOrder= Order_Detail::
                        join('product','product.id','=','order_detail.product_id')
                        ->select('product.product_name','order_detail.jumlah','order_detail.harga')
                        ->where('order_detail.order_id',$id)
                        ->get();

        dd($lihatOrder);
        // return view('');
    }
    public function UploadValidasi(){
		$getIdOrder = order::join('users','users.id','=','order.user_id')
					->Select('order.id','users.name','users.email','order.metode_pembayaran','order.total_harga','order.status_order','order.created_at')
					->where('users.id',Auth::user()->id)
					->get();
					// dd($getIdOrder); 
		return view('frontEnd/validasi',compact('getIdOrder'));
		
	}

    
    

}
