<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cart;
use App\Produk;
use Illuminate\Support\Facades\Auth;

class KeranjangController extends Controller
{
    public function index(){
        $showKeranjang = Cart:: join('product','Product.id','=','Cart.product_id')
                             -> select('Cart.id','Cart.total','Product.product_name','Product.product_img','Product.price')->get();
                            //  dd($showKeranjang);
        $hargabarang = 0;
        foreach($showKeranjang as $show){
            $harga = $show-> price * $show->total;
            $hargabarang = $harga+$hargabarang;
        }
        
        return view('frontEnd/cart',compact('showKeranjang','hargabarang'));
    }

    public function tambah(Request $request){
        $keranjang = new Cart;
        
        $keranjang->product_id = $request->product_id;
        $keranjang->user_id = Auth::id();
        $keranjang->total = $request->qty;
        $keranjang->save();

        return redirect()->back();

    }
    public function destroy($id)
    {
        $keranjang = new Cart;
        $keranjang->destroy($id);
        

        return redirect()->back();
    }
}
