<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Illuminate\Support\Facades\Crypt;


class KategoriController extends Controller
{
    public function index(Request $request)
    {
        $kategori = Category::select('id','category_name')->paginate(5);
        return view('/admin/kategori', compact('kategori'));
    }
    public function tambah(Request $request)
    {
        $newCategories = new Category;
        $newCategories-> category_name = $request->categoryName;
        $newCategories ->save();
        return redirect()->back()->with('success', 'berhasil Input Data!');
    }
    public function delete(Request $request)
    {
        $id = $request->deleteCategoriesId;
        // dd($id);
        $deleteFile = Category::where('id',$id)->delete();
        if($deleteFile){
            return [
                'result' => 'success',
                'message' => 'Berhasil menghapus data!',
            ];
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat menghapus data!',
            ];
        }
        return redirect()->back();
    }
}
