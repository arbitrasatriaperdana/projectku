<?php

namespace App\Http\Controllers;
use App\Users;
use Redirect;

use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('loginuser');
    }
    public function loginAdmin(Request $request)
    {
        $email      =       $request->email;
        $password   =       $request->password;
        $loginUser  =       Users::where('email', $email)->where('password', $password)->first();
        if($loginUser = true)
        {
           session(['login_success'=>true]);
            $url    =       'http://127.0.0.1:8000/home';
            return redirect :: to($url)->with('success','Selamat Datang');
        }
        return redirect()->back()->with('error','Data salah');
    }
    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/login');//
    }

}
