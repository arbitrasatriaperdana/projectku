<?php

namespace App\Http\Controllers;
use App\Produk;
use App\Promo;
use App\Media;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function show()
    {
        $produkshow= Produk::select("id","product_name","product_desc","product_category","qty","price","product_img","age")->get();
        // dd($produkshow);
        $promoshow = Promo::select("id","promo_img")->orderBy("id","DESC")->limit(2)->get();
        // dd($promoshow);
        return view('/frontEnd/dashboard',compact('produkshow','promoshow')) ;
    }
    public function productdetail($id)
    {
        $Produkdetail = Produk::FindorFail($id);
        return view('/frontEnd/detail_item',compact('Produkdetail'));
    }
    public function listproductBlueRim(){
        $produklist= Produk::select("id","product_name","product_desc","product_category","qty","price","product_img")
                             ->where("product_category","=","Blue Rim")
                             ->get();
        return view('/frontEnd/product_list',compact('produklist')) ;
    }
    public function listRedKoi()
    {
        $produklist= Produk::select("id","product_name","product_desc","product_category","qty","price","product_img")
        ->where("product_category","=","Red Koi Choper")
        ->get();
        return view('/frontEnd/product_list',compact('produklist')) ;
    }
    public function listAvatar()
    {
        $produklist= Produk::select("id","product_name","product_desc","product_category","qty","price","product_img")
        ->where("product_category","=","Avatar")
        ->get();
        return view('/frontEnd/product_list',compact('produklist')) ;
    }
}
