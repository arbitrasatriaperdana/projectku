<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Produk;
use App\Media;
use File;
Use \Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class ProductController extends Controller
{
    public function index(request $request)
    {
        $produk = Produk::select('id','product_name','product_category','qty','price','product_desc','product_img','age')->paginate(5);
        return view('admin/produk/produk',compact('produk'));
    }
    public function create(Request $request)
    {
        $productName = $request->productName;
        $image = $request->productImg;
        $CategoryuImg = $request->productCategory;
        $originalName = $image->getClientOriginalName();
        $timestamp = Carbon::now()->format('YmdHisu'); 
        $timeOriginalName = $timestamp.'_'.$originalName;
        $image->move(public_path()."/gambar", $timeOriginalName);

        $newProduct = new Produk;
        $newProduct->product_name       = $request->productName;
        $newProduct->product_category   = $request->productCategory;
        $newProduct->product_desc       = $request->productDesc;
        $newProduct->qty                = $request->qty;
        $newProduct->price              = $request->price;
        $newProduct->age                = $request->age;
        $newProduct->product_img        = $timeOriginalName;
        $newProduct -> save(); 
        $newImage = new Media;
        $newImage->original_name = $timeOriginalName;
        $newImage->image_name = $productName;
        $newImage->image_category = $CategoryuImg;
        $newImage->save();
        return redirect()->back()->with('success', 'berhasil Input Data!');
    }
    public function destroy(Request $request)
    {
        $id = $request->deleteProductId;
        // dd($id);
        $selectedData = produk::where('id', $id)->first();
        $imageName = $selectedData->product_img;
        $oldDestinationPath = public_path().'/gambar/'.$imageName;
        $newDestinationPath = public_path().'/delete/'.$imageName;
        $move = File::move($oldDestinationPath, $newDestinationPath);
        if($move){
            $deleteFile = produk::where('id',$id)->delete();
            if($deleteFile){
                return [
                    'result' => 'success',
                    'message' => 'Berhasil menghapus data!',
                ];
            }else{
                return [
                    'result' => 'error',
                    'message' => 'Error saat menghapus data!',
                ];
            }
        }else{
            return [
                'result' => 'error',
                'message' => 'Error saat menghapus gambar!',
            ];
        }
        return redirect()->back();
    }
    public function update(request $request,$id)
    {
        $prevImage = $request->prevImage;
        $oldDestinationPath = public_path().'/gambar/'.$prevImage;
        $newDestinationPath = public_path().'/delete/'.$prevImage;
        if($oldDestinationPath == true){
            $move = File::move($oldDestinationPath, $newDestinationPath);
            // dd($move);
        }else{
            return [
                'result' => 'error',
                'message' => 'Kesalahan saat mengedit gambar!',
            ];
        }
        $image = $request->productImg;
        $originalName = $image->getClientOriginalName();
        $timestamp = Carbon::now()->format('YmdHisu'); 
        $timeOriginalName = $timestamp.'_'.$originalName;
        $moved = $image->move(public_path()."/gambar", $timeOriginalName);
        $editProduct = produk::find($id);
        $editProduct -> product_name        =   $request->productName;
        $editProduct -> product_category    =   $request->ProductCategory;
        $editProduct -> product_desc        =   $request->productDesc;
        $editProduct -> qty                 =   $request->qty;
        $editProduct -> age                 =   $request->age;
        $editProduct -> price               =   $request->price;
        $editProduct -> product_img         =   $timeOriginalName;
        $editProduct ->save();
        return redirect()->back()->with('success', 'berhasil edit Data Produk!');
    }
    public function Cart($request)
    {
        
    }
}
