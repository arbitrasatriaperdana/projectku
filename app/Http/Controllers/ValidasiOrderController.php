<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\ValidasiOrderMail;
use App\Order;
use App\Order_Detail;
use App\User;
use App\Produk;
use Illuminate\Support\Facades\Mail;

class ValidasiOrderController extends Controller
{
    public function index($id){
		

		$order= Order:: join('users','users.id','=','order.user_id')
                    ->Select('order.id','users.name','users.email','order.metode_pembayaran','order.total_harga','order.status_order')
                    ->where('order.id',$id)
					->first();
		
		$detailOrder= Order_Detail::join('product','Order_detail.product_id','=','product.id')
					->where('Order_detail.order_id',$id)
					->select('Order_detail.jumlah','product.product_name','product.price','product.id')
					->get();

		// dd($detailOrder);
		// return view('EmailValidasi',compact('order', 'detailOrder'));
		Mail::to($order->email)->send(new ValidasiOrderMail($order,$detailOrder));
 
		return "Email telah dikirim";
 
	}

}
