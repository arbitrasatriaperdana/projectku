<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $user = Users :: select('id','username','password','first_name','last_name','no_telp','email')->paginate(5);
        return view('/admin/user',compact('user'));
    }
    public function loginAdmin(Request $request)
    {
        $email      =       $request->email;
        $password   =       $request->password;
        $loginUser  =       Users::where('email', $email)->where('password', $password)->first();
        if($loginUser = true)
        {
            $url    =       'http://127.0.0.1:8000/dashboard/';
            return redirect :: to($url)->with('success','Selamat Datang');
        }
        return redirect()->back()->with('error','Data salah');
    }
    public function view()
    {

        $viewUser = Users :: select ('id','name','password','first_name','last_name','no_telp','email')
                            ->where('id',Auth::user()->id)->get();
                            // dd($viewUser);
        return view('/frontEnd/user',compact('viewUser'));

    }
}
