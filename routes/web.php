<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/detailorder', function () {
    return view('admin/orderdetail');
});

Route::get('/','MainController@show')->name('dashboard');

Auth::routes();
Route::get('/orderAdmin','OrderController@viewAdmin');
Route::get('/home','MainController@show')->name('dashboard');
Route::get('/detail/{id}','MainController@productdetail');
Route::get('/product','MainController@listproductBlueRim');
Route::get ('/redkoi','MainController@listRedKoi');
Route::get ('/avatar','MainController@listAvatar');
// // product
Route::group(['middleware'=>'LoginAdminMiddleware'],function(){
Route::get('/produk','ProductController@index');
Route::post('/produk/post','ProductController@create')->name('create.product');
Route::delete('/produk/delete/{id}','ProductController@destroy')->name('delete.product');
Route::post('/produk/update/{id}','ProductController@update')->name('update.product');
//end product
//order
Route::get('/order','ProductController@index');
//end order
//category
Route::get('/kategori','KategoriController@index');
Route::post('/kategori/post','KategoriController@tambah');
Route::delete('/kategori/delete/{id}','KategoriController@delete');
//end category
//home admin
Route::get('/dashboard','AdminController@view');
Route::get('/logout','AdminController@logoutAdmin')->name('logoutadmin');
//promo
Route:: get('/promo','PromoController@index');
Route:: post('/promo/add','PromoController@tambah');
Route:: delete('/promo/delete/{id}','PromoController@destroy');
//end promos
});
//end home admin
//login admin
Route::get('/loginAdmin','AdminController@viewLogin');
Route::get('/login/admin','AdminController@loginAdmin');
//end login admin
//user
Route:: get('/user','UserController@index');

//keranjang
Route:: get('/keranjang','KeranjangController@index');
Route:: post('/keranjang/add','KeranjangController@tambah')->name('keranjang.add');
Route:: DELETE('/keranjang/delete/{id}','KeranjangController@destroy')->name('keranjang.delete');
//end keranjang

//Order
Route:: get('/order','OrderController@index');
Route:: get('/order/viewadd',"OrderController@viewtambah")->name('order.viewadd');
Route:: post('/ordersuccess','OrderController@tambah')->name('order.success');
Route:: get('/detailOrder/{id}','OrderController@viewDetail')->name('order.viewDetail');
//end Order
//history order
Route:: get('/history','OrderController@history');
Route:: get('/history/detail/{id}','OrderController@history_detail');
//end history order
//validasi
Route::get('/validasi', 'OrderController@UploadValidasi'); 
Route::get('/alamatuser', function () {
    return view('frontEnd/alamatUser');
});
//end validasi
//about
Route::get('/about', function () {
    return view('frontEnd/about');
});
//end about



// Route:: get('/logout','LoginController@logout');

// Route:: get('/login','LoginController@index');
Route:: get('/proses','LoginController@loginAdmin');
//end user
//user page
Route::get('/user','UserController@View');
//end user page
//user view
//email
Route:: get('/validasiemail/{id}','ValidasiOrderController@index');
Route:: get('/validasipengiriman/{id}','ValidasiPengirimanController@index');
//end email



