//JS DELETE PRODUCT
$(document).on('click', '.deleteProduct', function(){
    var csrfToken         = $('meta[name="csrf-token"]').attr('content');
    var deleteProductId      = $(this).closest("td").find('.deleteProductId').val();
    alert(deleteProductId)
    Swal.fire({
      title               : 'Apakah anda yakin ingin menghapus file?',
      text                : 'Data akan di hapus dari Database!',
      icon                : 'warning',
      showCancelButton    : true,
      confirmButtonColor  : '#d33',
      cancelButtonColor   : '#3085d6',
      confirmButtonText   : 'Delete!'
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type                : "DELETE",
          url                 : "/produk/delete/{id}",
          data:{
            '_method'         : 'DELETE',
            '_token'          : csrfToken,
            'deleteProductId'    : deleteProductId,
          },
          success: function(data){
            console.log(data)
            if(data.result == 'success'){
              Swal.fire(
                'Success!',
                data['message'],
                'success'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }else{
              Swal.fire(
                'Error!',
                data['message'],
                'error'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }
          }
          
        })
      }
    })
  })
  //END JS DELETE PRODUCT

  //BEGIN JS DELETE CATEGORIES
  $(document).on('click', '.deleteCategories', function(){
    var csrfToken           = $('meta[name="csrf-token"]').attr('content');
    var deleteCategoriesId   = $(this).closest("td").find('.deleteCategoriesId').val();
    alert(deleteCategoriesId)
    Swal.fire({
      title               : 'Apakah anda yakin ingin menghapus file?',
      text                : 'Data akan di hapus dari Database!',
      icon                : 'warning',
      showCancelButton    : true,
      confirmButtonColor  : '#d33',
      cancelButtonColor   : '#3085d6',
      confirmButtonText   : 'Delete!'
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type                   : "DELETE",
          url                    : "/kategori/delete/{id}",
          data:{
            '_method'            : 'DELETE',
            '_token'             : csrfToken,
            'deleteCategoriesId'  : deleteCategoriesId,
          },
          success: function(data){
            console.log(data)
            if(data.result == 'success'){
              Swal.fire(
                'Success!',
                data['message'],
                'success'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }else{
              Swal.fire(
                'Error!',
                data['message'],
                'error'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }
          }
          
        })
      }
    })
  })
  //END JS DELETE CATEGORIES
   //JS DELETE PROMO
  $(document).on('click', '.deletePromo', function(){
    var csrfToken           = $('meta[name="csrf-token"]').attr('content');
    var deletePromoId   = $(this).closest("td").find('.deletePromoId').val();
    alert(deletePromoId)
    Swal.fire({
      title               : 'Apakah anda yakin ingin menghapus file?',
      text                : 'Data akan di hapus dari Database!',
      icon                : 'warning',
      showCancelButton    : true,
      confirmButtonColor  : '#d33',
      cancelButtonColor   : '#3085d6',
      confirmButtonText   : 'Delete!'
    }).then((result) => {
      if (result.isConfirmed) {
        $.ajax({
          type                   : "DELETE",
          url                    : "/promo/delete/{id}",
          data:{
            '_method'            : 'DELETE',
            '_token'             : csrfToken,
            'deletePromoId'  : deletePromoId,
          },
          success: function(data){
            console.log(data)
            if(data.result == 'success'){
              Swal.fire(
                'Success!',
                data['message'],
                'success'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }else{
              Swal.fire(
                'Error!',
                data['message'],
                'error'
              )
              // BEGIN::Reload page
              .then((result)=>{
                location.reload();
              })
              // END::Reload page
            }
          }
          
        })
      }
    })
  })
 

  //END JS DELETE PROMO